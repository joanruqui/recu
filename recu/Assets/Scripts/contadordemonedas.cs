using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class contadordemonedas : MonoBehaviour
{
    public float bank;
    public Text banktext;
    public static contadordemonedas instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        banktext.text = bank.ToString();
    }

    public void Money(float cashCollected)
    {
        bank += cashCollected;
        banktext.text = bank.ToString();
    }

    // Update is called once per frame
    void Update()
    {





    }

}
