using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class plataforma : MonoBehaviour
{
    private GameObject sitio;
    public float min;
    public float max;
    public float Velocidad = 1f;
    public float cooldown;
    void Start()
    {
        mover();
        StartCoroutine("movement");
    }
    void Update()
    {

    }
    private void mover()
    {
        if (sitio == null)
        {
            sitio = new GameObject("Sitio_objetivo");
            sitio.transform.position = new Vector2(min,
            transform.position.y);
            transform.localScale = new Vector3(-1, 1, 1);
            return;
        }
        if (sitio.transform.position.x == min)
        {
            sitio.transform.position = new Vector2(max,
            transform.position.y);
            transform.localScale = new Vector3(1, 1, 1);
        }
        else if (sitio.transform.position.x == max)
        {
            sitio.transform.position = new Vector2(min,
            transform.position.y);
            transform.localScale = new Vector3(-1, 1, 1);
        }
    }
    private IEnumerator movement()
    {
        while (Vector2.Distance(transform.position, sitio.transform.position)
        > 0.05f)
        {
            Vector2 direction = sitio.transform.position -
            transform.position;
            float xDirection = direction.x;
            transform.Translate(direction.normalized * Velocidad *
            Time.deltaTime);
            yield return null;
        }
        transform.position = new Vector2(sitio.transform.position.x,
        transform.position.y);
        yield return new WaitForSeconds(cooldown);
        mover();
        StartCoroutine("movement");
    }

}
