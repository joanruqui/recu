using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Playercontoller : MonoBehaviour
{
    public float velocidad;
    public float fuerzaSalto;
    public LayerMask capasuelo;
    public int saltosmaximos;

    private Rigidbody2D rigidBody;
    private bool mirandoDerecha = true;
    private Animator animator;
    private BoxCollider2D boxcollider;
    private int saltosrestantes;
    void Start()
    {

        rigidBody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        boxcollider = GetComponent<BoxCollider2D>();
        saltosrestantes = saltosmaximos;
    }

    // Update is called once per frame
    void Update()
    {
        ProcesoMovimiento();
        ProcesoSalto();

    }
    bool EstaEnSuelo()

    {
        RaycastHit2D raycastHit = Physics2D.BoxCast(boxcollider.bounds.center, new Vector2(boxcollider.bounds.size.x, boxcollider.bounds.size.y), 0f, Vector2.down, 0.02f, capasuelo);
        return raycastHit.collider != null;

    }
    void ProcesoSalto()
    {
        if (EstaEnSuelo())
        {

            saltosrestantes = saltosmaximos;
        }

        if (Input.GetKeyDown(KeyCode.Space) && saltosrestantes > 0)
        {
            saltosrestantes = saltosrestantes - 1;
            rigidBody.velocity = new Vector2(rigidBody.velocity.x, 0f);
            rigidBody.AddForce(Vector2.up * fuerzaSalto, ForceMode2D.Impulse);

        }
    }

    void ProcesoMovimiento()
    {
        float inputMovimiento = Input.GetAxis("Horizontal");

        if (inputMovimiento != 0f)
        {
            animator.SetBool("run", true);

        }
        else
        {
            animator.SetBool("run", false);
        }


        rigidBody.velocity = new Vector2(inputMovimiento * velocidad, rigidBody.velocity.y);

        GestionarOrientacion(inputMovimiento);


    }
    void GestionarOrientacion(float inputMovimiento)
    {
        if ((mirandoDerecha == true && inputMovimiento < 0) || (mirandoDerecha == false && inputMovimiento > 0))
        {
            mirandoDerecha = !mirandoDerecha;
            transform.localScale = new Vector2(-transform.localScale.x, transform.localScale.y);

        }


    }
}